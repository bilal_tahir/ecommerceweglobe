<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.home');
});

// Route::get('/shop', function () {
//     return view('website.shop');
// });

// Route::get('/description', function () {
//     return view('website.product_description');
// });

Route::get('/blog', function () {
    return view('website.blogs');
});

Route::get('/blog_detail', function () {
    return view('website.blog_detail');
});

Route::get('/faq', function () {
    return view('website.faq');
});

Route::get('/contactus', function () {
    return view('website.contact');
});

Route::get('/aboutus', function () {
    return view('website.about');
});

Route::get('/vendor', function () {
    return view('website.vendor');
});

Route::get('/store', function () {
    return view('website.stores');
});

Route::get('/tracking', function () {
    return view('website.tracking');
});

Route::get('/cart', function () {
    return view('website.cart');
});

Route::get('/checkout', function () {
    return view('website.checkout');
});

Route::get('debug','DebugController@index');

Route::get('/shop','ShopController@shop');

Route::get('/shop/product/{slug}', 'ShopController@productshow')->name('shop.product');