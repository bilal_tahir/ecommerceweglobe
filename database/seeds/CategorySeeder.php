<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Consumer Electronics',
            'slug' =>'electronics',
            'description' => 'This category is for electronic products',
        ]);

         Category::create([
            'name' => 'Clothing & Apparel',
            'slug' =>'clothing',
            'description' => 'This category is for clothing products',
        ]);

         Category::create([
            'name' => 'Home Garden & Kitchens',
            'slug' =>'household',
            'description' => 'This category is for household products',
        ]);

         Category::create([
            'name' => 'Health & Beauty',
            'slug' =>'herbs',
            'description' => 'This category is for herbs products',
        ]);

         Category::create([
            'name' => 'Furniture',
            'slug' =>'jewellery',
            'description' => 'This category is for Jewellery products',
        ]);

         Category::create([
            'name' => 'Computer & Technology',
            'slug' =>'computers',
            'description' => 'This category is for computer products',
        ]);

        Category::create([
            'name' => 'Organic',
            'slug' =>'organic',
            'description' => 'This category is for sports products',
        ]);

        
    }
}
