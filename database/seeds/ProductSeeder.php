<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Brand;
use App\Color;
use App\Size;
use App\Category;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        // Seeding for clothing

        for ($i=1; $i < 8 ; $i++) { 

        $product = Product::create([
            'name' => 'Cloth'.$i,
            'thumbnail' => 'cloth'.$i,
            'image1' => 'cloth'.$i,
            'image2' => 'cloth'.$i,
            'image3' => 'cloth'.$i,
            'slug' => 'cloth'.$i,
            'price1' => rand(10,50),
            'price2' => rand(50,100),
            'quantity' => rand(10,50),
            'sku' => 'cloth'.rand(1000,10000),

        ]);

            $product->brands()->attach(1);
            $product->colors()->attach([1,2,3]);
            $product->size()->attach([1,2,3,4]);
            $product->category()->attach(2);


        }

        //Seeding For Electronics
        for ($i=1; $i < 11 ; $i++) { 

        $product = Product::create([
            'name' => 'Electronic'.$i,
            'thumbnail' => 'cloth'.$i,
            'image1' => 'cloth'.$i,
            'image2' => 'cloth'.$i,
            'image3' => 'cloth'.$i,
            'slug' => 'electronic'.$i,
            'price1' => rand(10,50),
            'price2' => rand(50,100),
            'quantity' => rand(10,50),
            'sku' => 'electric'.rand(1000,10000),

        ]);

            $product->brands()->attach(2);
            $product->colors()->attach([1,3]);
           
            $product->category()->attach(1);


        }
        for ($i=11; $i < 21 ; $i++) { 

        $product = Product::create([
            'name' => 'Electronic'.$i,
            'thumbnail' => 'cloth'.$i,
            'image1' => 'cloth'.$i,
            'image2' => 'cloth'.$i,
            'image3' => 'cloth'.$i,
            'slug' => 'electronic'.$i,
            'price1' => rand(10,50),
            'price2' => rand(50,100),
            'quantity' => rand(10,50),
            'sku' => 'electric'.rand(1000,10000),

        ]);

            $product->brands()->attach(4);
            $product->colors()->attach([1,2]);
           
            $product->category()->attach(6);


        }

        //Seeding For Furniture

    for ($i=1; $i < 14 ; $i++) { 

        $product = Product::create([
            'name' => 'Furniture'.$i,
            'thumbnail' => 'cloth'.$i,
            'image1' => 'cloth'.$i,
            'image2' => 'cloth'.$i,
            'image3' => 'cloth'.$i,
            'slug' => 'furniture'.$i,
            'price1' => rand(200,300),
            'price2' => rand(300,400),
            'quantity' => rand(10,50),
            'sku' => 'fur'.rand(1000,10000),

        ]);

            $product->brands()->attach(3);
            $product->colors()->attach([1,2,3]);
           $product->size()->attach([2,4]);
            $product->category()->attach(5);


        }

        //Seeding for Health & BBeauty
        
    for ($i=1; $i < 7 ; $i++) { 

        $product = Product::create([
            'name' => 'Health & Beauty'.$i,
            'thumbnail' => 'cloth'.$i,
            'image1' => 'cloth'.$i,
            'image2' => 'cloth'.$i,
            'image3' => 'cloth'.$i,
            'slug' => 'h&b'.$i,
            'price1' => rand(100,150),
            'price2' => rand(151,250),
            'quantity' => rand(10,50),
            'sku' => 'bea'.rand(1000,10000),

        ]);

            $product->brands()->attach(7);
            $product->colors()->attach([1,2,3]);
           $product->size()->attach([2,4]);
            $product->category()->attach(4);


        }

        //Seeding for Organic
         for ($i=1; $i < 11 ; $i++) { 

        $product = Product::create([
            'name' => 'Organic'.$i,
            'thumbnail' => 'cloth'.$i,
            'image1' => 'cloth'.$i,
            'image2' => 'cloth'.$i,
            'image3' => 'cloth'.$i,
            'slug' => 'organic'.$i,
            'price1' => rand(100,150),
            'price2' => rand(151,250),
            'quantity' => rand(10,50),
            'sku' => 'org'.rand(1000,10000),

        ]);

            $product->brands()->attach(6);
            // $product->colors()->attach([1,2,3]);
           $product->size()->attach([1,2,3]);
            $product->category()->attach(5);


        }

        //Seeding for tech

           for ($i=1; $i < 23 ; $i++) { 

        $product = Product::create([
            'name' => 'Tech'.$i,
            'thumbnail' => 'cloth'.$i,
            'image1' => 'cloth'.$i,
            'image2' => 'cloth'.$i,
            'image3' => 'cloth'.$i,
            'slug' => 'tech'.$i,
            'price1' => rand(200,250),
            'price2' => rand(251,500),
            'quantity' => rand(10,50),
            'sku' => 'org'.rand(1000,10000),

        ]);

            $product->brands()->attach(2);
            $product->colors()->attach([1,2,3]);
        //    $product->size()->attach([1,2,3]);
            $product->category()->attach(6);


        }

    }
}
