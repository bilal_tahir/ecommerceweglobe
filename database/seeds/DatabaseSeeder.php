<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
            $this->call(BrandSeeder::class);
            $this->call(ColorSeeder::class);
            $this->call(SizeSeeder::class);
            $this->call(CategorySeeder::class);
            $this->call(MenuSeeder::class);
            $this->call(ProductSeeder::class);
             $this->call(DescriptionSeeder::class);
                $this->call(TagSeeder::class);
                   $this->call(SpecificationSeeder::class);
    }
}
