<?php

use Illuminate\Database\Seeder;
use App\Image;
class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Image::create([
              Size::create([
            'name' => 'Extra Large',
            'size' => 'XL',
            'slug' => 'extra_large',
            'description' => 'This is Extra Large Size.',
        ]);
        ]);
    }
}
