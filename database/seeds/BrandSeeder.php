<?php

use Illuminate\Database\Seeder;
use App\Brand;


class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Brand::create([
          'name' => 'Adidas',
          'slug' => 'adidas',
          'description' =>'This brand is of Adidas.',
      ]);

       Brand::create([
          'name' => 'Apple',
          'slug' => 'apple',
          'description' =>'This brand is of Apple.',
      ]);

       Brand::create([
          'name' => 'Casio',
          'slug' => 'casio',
          'description' =>'This brand is of Casio.',
      ]);

       Brand::create([
          'name' => 'Electrolux',
          'slug' => 'electrolux',
          'description' =>'This brand is of Electrolux.',
      ]);

       Brand::create([
          'name' => 'Samsung',
          'slug' => 'samsung',
          'description' =>'This brand is of Samsung.',
      ]);

      
       Brand::create([
          'name' => 'Food',
          'slug' => 'food',
          'description' =>'This brand is of Food.',
      ]);

      
       Brand::create([
          'name' => 'Cosmetics',
          'slug' => 'cosmetics',
          'description' =>'This brand is of Samsung.',
      ]);
    }
}
