<?php

use Illuminate\Database\Seeder;
use App\Size;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Size::create([
            'name' => 'Small',
            'size' => 'S',
            'slug' => 'small',
            'description' => 'This is Small Size.',
        ]);

        Size::create([
            'name' => 'Medium',
            'size' => 'M',
            'slug' => 'medium',
            'description' => 'This is Medium Size.',
        ]);

        Size::create([
            'name' => 'Large',
            'size' => 'L',
            'slug' => 'large',
            'description' => 'This is Large Size.',
        ]);

        Size::create([
            'name' => 'Extra Large',
            'size' => 'XL',
            'slug' => 'extra_large',
            'description' => 'This is Extra Large Size.',
        ]);
    }
}
