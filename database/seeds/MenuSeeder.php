<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::Create([
            'name' => 'Home',
        ]);

        Menu::Create([
            'name' => 'Shop',
        ]);

        Menu::Create([
            'name' => 'About Us',
        ]);

        Menu::Create([
            'name' => 'Blogs',
        ]);

         Menu::Create([
            'name' => 'Contact',
        ]);
    }
}
