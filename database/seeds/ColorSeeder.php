<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Color::create([
           'name' => 'Black',
           'slug' => 'black',
           'description' => 'This is black color.',
           'value' => '#000;',
       ]);


         Color::create([
           'name' => 'White',
           'slug' => 'white',
           'description' => 'This is white color.',
           'value' => '#fff;',
       ]);

         Color::create([
           'name' => 'Yellow',
           'slug' => 'yellow',
           'description' => 'This is Yellow color.',
           'value' => '#ffc107;',
       ]);
    }
}
