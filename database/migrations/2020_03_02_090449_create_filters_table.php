<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
        $table->unsignedbigInteger('product_id')->nullable();
        $table->unsignedbigInteger('category_id')->nullable();
        $table->unsignedbigInteger('brand_id')->nullable();
        $table->unsignedbigInteger('color_id')->nullable();
        $table->unsignedbigInteger('size_id')->nullable();


         $table->foreign('product_id')->references('id')->on('products');

            $table->foreign('category_id')->references('id')->on('categories');

             $table->foreign('brand_id')->references('id')->on('brands');

            $table->foreign('color_id')->references('id')->on('colors');

             $table->foreign('size_id')->references('id')->on('sizes');

        

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
}
