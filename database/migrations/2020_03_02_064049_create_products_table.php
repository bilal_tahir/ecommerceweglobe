<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            
            $table->string('thumbnail')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();

            $table->string('slug')->nullable()->unique();
            $table->float('price1')->nullable();
            $table->float('price2')->nullable();
            $table->float('discount')->default(0);
            $table->float('discountprice')->default(0);
            $table->unsignedInteger('quantity')->nullable();
            $table->boolean('sale')->default(False);
            $table->boolean('feature')->default(False);
            $table->boolean('publish')->default(False);
            $table->boolean('outofstock')->default(False);
            $table->string('sku')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
