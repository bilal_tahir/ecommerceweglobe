<?php

namespace App\Http\Controllers;

use App\Levelonecategory;
use Illuminate\Http\Request;

class LevelonecategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Levelonecategory  $levelonecategory
     * @return \Illuminate\Http\Response
     */
    public function show(Levelonecategory $levelonecategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Levelonecategory  $levelonecategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Levelonecategory $levelonecategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Levelonecategory  $levelonecategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Levelonecategory $levelonecategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Levelonecategory  $levelonecategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Levelonecategory $levelonecategory)
    {
        //
    }
}
