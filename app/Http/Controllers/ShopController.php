<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use App\Brand;
use App\Category;
use App\Product;
use App\Size;


class ShopController extends Controller
{

   

    public function shop()
    {
            $color = Color::all();
            $brand = Brand::all();
            $size = Size::all();
            $category = Category::all();
            $product = new Product();
            $Product_publish = $product->where('publish', False);
            $items = $Product_publish->paginate(12);
            return view ('website.shop')->with([
                'color' => $color ,
                'brand' => $brand ,
                'size' => $size ,
                'category' => $category ,
                'items' => $items ,
            ]);

    }

    public function productshow($slug)
    {
            // $color = Color::all();
            // $brand = Brand::all();
            // $size = Size::all();
            // $category = Category::all();


            $product = new Product();

            $Product_publish = $product->where('slug' , $slug)->first();
            

        //dd($Product_publish->brands->name);
     
            return view ('website.product_description')->with([
                // 'color' => $color ,
                // 'brand' => $brand ,
                // 'size' => $size ,
                // 'category' => $category ,
                'Product_publish' => $Product_publish ,
            ]);

    }
}
