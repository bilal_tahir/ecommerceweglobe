<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Database\Eloquent\Model;
use App\Specification;
use App\Brand;
use App\Category;
use App\Color;
use App\Size;
use App\Description;

class Product extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];


    public function brands()
    {
        return $this->belongsToMany(Brand::class, 'filters');
    }

    
    public function category()
    {
        return $this->belongsToMany(Category::class, 'filters');
    }

    
    public function colors()
    {
        return $this->belongsToMany(Color::class, 'filters');
    }

    
    public function size()
    {
        return $this->belongsToMany(Size::class, 'filters');
    }

    public function description(){

        return $this->hasOne(Description::class , 'product_id' );
    }
    
    public function specification(){

        return $this->hasOne(Specification::class , 'product_id' );
    }
}
