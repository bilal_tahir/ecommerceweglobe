<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Category extends Model
{
    public function product()
    {
        return $this->belongsToMany(Product::class, 'filters');
    }
}
