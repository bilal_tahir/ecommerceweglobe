@extends('website_layout.main')
 {{-- @section('title')
 Project | Solved Engineering
 @endsection --}}
 @section('content')
 <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header">
                <h1>Our Press</h1>
                <div class="ps-breadcrumb--2">
                    <ul class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li>Our Press</li>
                    </ul>
                </div>
            </div>
            <div class="ps-blog">
                <div class="ps-blog__header">
                    <ul class="ps-list--blog-links">
                        <li class="active"><a href="blog-list.html">All</a></li>
                        <li><a href="blog-list.html">Life Style</a></li>
                        <li><a href="blog-list.html">Technology</a></li>
                        <li><a href="blog-list.html">Entertaiment</a></li>
                        <li><a href="blog-list.html">Business</a></li>
                        <li><a href="blog-list.html">Others</a></li>
                        <li><a href="blog-list.html">Fashion</a></li>
                    </ul>
                </div>
                <div class="ps-blog__content">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                            <div class="ps-post">
                            <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="{{url('blog_detail')}}"></a><img src="img/blog/grid/1.jpg" alt="">
                                    <div class="ps-post__badge"><i class="icon-volume-high"></i></div>
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta"><a href="#">Entertaiment</a><a href="#">Technology</a>
                                    </div><a class="ps-post__title" href="#">Experience Great Sound With Beats’s Headphone</a>
                                    <p>December 17, 2017 by<a href="#"> drfurion</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                            <div class="ps-post">
                                <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="blog-detail.html"></a><img src="img/blog/grid/2.jpg" alt="">
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta"><a href="#">Life Style</a><a href="#">Others</a>
                                    </div><a class="ps-post__title" href="#">Products Necessery For Mom</a>
                                    <p>December 17, 2017 by<a href="#"> drfurion</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                            <div class="ps-post">
                                <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="blog-detail.html"></a><img src="img/blog/grid/3.jpg" alt="">
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta"><a href="#">Life Style</a><a href="#">Others</a>
                                    </div><a class="ps-post__title" href="#">Home Interior: Modern Style 2017</a>
                                    <p>December 17, 2017 by<a href="#"> drfurion</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                            <div class="ps-post">
                                <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="blog-detail.html"></a><img src="img/blog/grid/4.jpg" alt="">
                                    <div class="ps-post__badge"><i class="icon-picture"></i></div>
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta"><a href="#">Bussiness</a>
                                    </div><a class="ps-post__title" href="#">DeerCo – A New Look About Startup In Product Manufacture Field7</a>
                                    <p>December 17, 2017 by<a href="#"> drfurion</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                            <div class="ps-post">
                                <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="blog-detail.html"></a><img src="img/blog/grid/5.jpg" alt="">
                                    <div class="ps-post__badge"><i class="icon-camera-play"></i></div>
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta"><a href="#">Technology</a>
                                    </div><a class="ps-post__title" href="#">B&amp;O Play – Best Headphone For You</a>
                                    <p>December 17, 2017 by<a href="#"> drfurion</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                            <div class="ps-post">
                                <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="blog-detail.html"></a><img src="img/blog/grid/6.jpg" alt="">
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta"><a href="#">Businesses</a><a href="#">Life Style</a>
                                    </div><a class="ps-post__title" href="#">Unique Products For Your Kitchen From IKEA Design</a>
                                    <p>December 17, 2017 by<a href="#"> drfurion</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ps-pagination">
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">Next Page<i class="icon-chevron-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ps-newsletter">
        <div class="ps-container">
            <form class="ps-form--newsletter" action="http://nouthemes.net/html/martfury/do_action" method="post">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__left">
                            <h3>Newsletter</h3>
                            <p>Subcribe to get information about products and coupons</p>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__right">
                            <div class="form-group--nest">
                                <input class="form-control" type="email" placeholder="Email address">
                                <button class="ps-btn">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
   
 @endsection