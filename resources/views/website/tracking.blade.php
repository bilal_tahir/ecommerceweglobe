@extends('website_layout.main')
 {{-- @section('title')
 Project | Solved Engineering
 @endsection --}}
 @section('content')
    <div class="ps-page--simple">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="shop-default.html">Shop</a></li>
                    <li> Order Tracking</li>
                </ul>
            </div>
        </div>
        <div class="ps-order-tracking">
            <div class="container">
                <div class="ps-section__header">
                    <h3>Order Tracking</h3>
                    <p>To track your order please enter your Order ID in the box below and press the "Track" button. This was given to youon your receipt and in the confirmation email you should have received.</p>
                </div>
                <div class="ps-section__content">
                    <form class="ps-form--order-tracking" action="http://nouthemes.net/html/martfury/index.html" method="get">
                        <div class="form-group">
                            <label>Order ID</label>
                            <input class="form-control" type="text" placeholder="Found in your order confimation email">
                        </div>
                        <div class="form-group">
                            <label>Billing Email</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group">
                            <button class="ps-btn ps-btn--fullwidth">Track Your Order</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="ps-newsletter">
        <div class="container">
            <form class="ps-form--newsletter" action="http://nouthemes.net/html/martfury/do_action" method="post">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__left">
                            <h3>Newsletter</h3>
                            <p>Subcribe to get information about products and coupons</p>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__right">
                            <div class="form-group--nest">
                                <input class="form-control" type="email" placeholder="Email address">
                                <button class="ps-btn">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

 @endsection