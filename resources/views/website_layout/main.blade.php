<!doctype html>
<html lang="en">

@include('website_layout.head')

<body>



    @include('website_layout.header')
<!--Session Controller has no link -->
    <!-- Code for Session Model -->



    @yield('content')

    @include('website_layout.footer')

     
       <script src="{{asset('assets/plugins/email-decode.min.js')}}"></script>

       <script src="{{asset('assets/plugins/jquery-1.12.4.min.js')}}"></script>

       <script src="{{asset('assets/plugins/popper.min.js')}}"></script>

       <script src="{{asset('assets/plugins/owl-carousel/owl.carousel.min.js')}}"></script>

       <script src="{{asset('assets/plugins/bootstrap4/js/bootstrap.min.js')}}"></script>

       <script src="{{asset('assets/plugins/imagesloaded.pkgd.min.js')}}"></script>

       <script src="{{asset('assets/plugins/masonry.pkgd.min.js')}}"></script>

       <script src="{{asset('assets/plugins/isotope.pkgd.min.js')}}"></script>

       <script src="{{asset('assets/plugins/jquery.matchHeight-min.js')}}"></script>

       <script src="{{asset('assets/plugins/slick/slick/slick.min.js')}}"></script>

       <script src="{{asset('assets/plugins/jquery-bar-rating/dist/jquery.barrating.min.js')}}"></script>

       <script src="{{asset('assets/plugins/slick-animation.min.js')}}"></script>

       <script src="{{asset('assets/plugins/lightGallery-master/dist/js/lightgallery-all.min.js')}}"></script>

       <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

       <script src="{{asset('assets/plugins/sticky-sidebar/dist/sticky-sidebar.min.js')}}"></script>

        <script src="{{asset('assets/plugins/jquery.slimscroll.min.js')}}"></script>

        <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}"></script>

        <script src="{{asset('assets/plugins/gmap3.min.js')}}"></script>

        <script src="{{asset('assets/js/main.js')}}"></script>

    
   
    <!-- custom scripts-->
  
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxflHHc5FlDVI-J71pO7hM1QJNW1dRp4U&amp;region=GB"></script>
</body>


<!-- Mirrored from nouthemes.net/html/martfury/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Feb 2020 10:00:52 GMT -->
</html>