   <footer class="ps-footer">
        <div class="ps-container">
            <div class="ps-footer__widgets">
                <aside class="widget widget_footer widget_contact-us">
                    <h4 class="widget-title">Contact us</h4>
                    <div class="widget_content">
                        <p>090-3000-1310</p>
                        
                        <p>support@dinku.com.ng
                        </p>
                        <ul class="ps-list--social">
                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Categories</h4>
                    <ul class="ps-list--link">
                        <li><a href="#">All Categories</a></li>
                        <li><a href="#">Home </a></li>
                        <li><a href="#">Lifestyle</a></li>
                        <li><a href="#">Phones & Gadgets</a></li>
                        <li><a href="faqs.html">Electronics</a></li>
                        <li><a href="#">Babies & Kiddies</a></li>
                        <li><a href="faqs.html">Services</a></li>
                        <li><a href="#">Others</a></li>
                    
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Menu</h4>
                    <ul class="ps-list--link">
                        <li><a href="about-us.html">Home</a></li>
                        <li><a href="#">Shop</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Blogs</a></li>
                        <li><a href="contact-us.html">Contact Us</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Others</h4>
                    <ul class="ps-list--link">
                        <li><a href="#">Track Order</a></li>
                        <li><a href="checkout.html">Privacy Policy</a></li>
                        <li><a href="my-account.html">Payment Options</a></li>
                        <li><a href="shop-default.html">Terms of Use</a></li>
                        <li><a href="shop-default.html">Delivery</a></li>
                        <li><a href="shop-default.html">Return Policy</a></li>
                    </ul>
                </aside>
            </div>
            <div class="ps-footer__links">
                <p><strong>Consumer Electric:</strong><a href="#">Air Conditioners</a><a href="#">Audios &amp;
                        Theaters</a><a href="#">Car Electronics</a><a href="#">Office Electronics</a><a href="#">TV
                        Televisions</a><a href="#">Washing Machines</a>
                </p>
                <p><strong>Clothing &amp; Apparel:</strong><a href="#">Printers</a><a href="#">Projectors</a><a
                        href="#">Scanners</a><a href="#">Store &amp; Business</a><a href="#">4K Ultra HD TVs</a><a
                        href="#">LED TVs</a><a href="#">OLED TVs</a>
                </p>
                <p><strong>Home, Garden &amp; Kitchen:</strong><a href="#">Cookware</a><a href="#">Decoration</a><a
                        href="#">Furniture</a><a href="#">Garden Tools</a><a href="#">Garden Equipments</a><a
                        href="#">Powers And Hand Tools</a><a href="#">Utensil &amp; Gadget</a>
                </p>
                <p><strong>Health &amp; Beauty:</strong><a href="#">Hair Care</a><a href="#">Decoration</a><a
                        href="#">Hair Care</a><a href="#">Makeup</a><a href="#">Body Shower</a><a href="#">Skin
                        Care</a><a href="#">Cologine</a><a href="#">Perfume</a>
                </p>
                <p><strong>Jewelry &amp; Watches:</strong><a href="#">Necklace</a><a href="#">Pendant</a><a
                        href="#">Diamond Ring</a><a href="#">Sliver Earing</a><a href="#">Leather Watcher</a><a
                        href="#">Gucci</a>
                </p>
                <p><strong>Computer &amp; Technologies:</strong><a href="#">Desktop PC</a><a href="#">Laptop</a><a
                        href="#">Smartphones</a><a href="#">Tablet</a><a href="#">Game Controller</a><a href="#">Audio
                        &amp; Video</a><a href="#">Wireless Speaker</a><a href="#">Done</a>
                </p>
            </div>
            <div class="ps-footer__copyright">
                <p>© 2018 Martfury. All Rights Reserved</p>
                <p><span>We Using Safe Payment For:</span><a href="#"><img src="{{asset('assets/img/payment-method/1.jpg')}}" alt=""></a><a
                        href="#"><img src="{{asset('assets/img/payment-method/2.jpg')}}" alt=""></a><a href="#"><img
                            src="{{asset('assets/img/payment-method/3.jpg')}}" alt=""></a><a href="#"><img src="{{asset('assets/img/payment-method/4.jpg')}}"
                            alt=""></a><a href="#"><img src="{{asset('assets/img/payment-method/5.jpg')}}" alt=""></a></p>
            </div>
        </div>
    </footer>